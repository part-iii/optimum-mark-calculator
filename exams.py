#!/usr/bin/python3
import csv

def exam_data(filename:str) -> list:

    exams = []

    with open(filename) as f:

        data = csv.DictReader(f)

        # Format data and calculate weighted grade = grade * units
        for row in data:

            row['units'] = int(row['units'])
            row['grade'] = int(row['grade'])
            row['w_grade'] = row['grade']*row['units']

            exams.append(row)

    return exams

def total_units(exams: list) -> int:

    return sum(exam['units'] for exam in exams)

def data_check(exams: list):

    for i in exams:

        if i['units'] != 2 and i['units'] != 3:

            raise Exception(f"Warning! {i['exam']} has {i['units']} which is not allowed: the only allowed amounts are 2 or 3.")

        elif i['grade'] < 0 or i['grade'] > 100:

            raise Exception(f"Warning! Grades above 100 or below 0 are not possible, but {i['exam']} has a grade of {i['grade']}.")

    if (total_creds := total_units(exams)) > 19:

        raise Exception(f"Warning! You cannot take {total_units} units to exam: the maximum is 19.")

    elif total_creds < 12 and total_creds >= 0:

        print(f"The Faculty Board recommends submitting at least 12 units to obtain a pass. Currently, you are submitting \033[1m\033[4m{total_creds}\033[0m.")

    elif total_creds < 0:

        raise Exception(f"Modules cannot be worth negative units")

    return


def optimum_mark(exams:list) -> tuple:

    additional_info = "None of your exams will be disregarded to get your optimum mark.\n"

    # Get the total number of exams
    num_tot_exams = len(exams)

    # Check that not too many units are being submitted and that the data are otherwise in order
    data_check(exams)

    # Calculate the first mean mark (with all exams included)
    optimum_mark = sum(exam['w_grade'] for exam in exams) / max(17,sum(exam['units'] for exam in exams))

    # Calculate the mean marks with one exam removed each time
    for idx, exam in enumerate(exams):


        w_grade_less_one = sum(exams[jdx]['w_grade'] for jdx in range(num_tot_exams) if jdx != idx)

        units_less_one = max(17,sum(exams[jdx]['units'] for jdx in range(num_tot_exams) if jdx != idx))

        if (mean_mark := w_grade_less_one / units_less_one) > optimum_mark:

            optimum_mark = mean_mark

            additional_info = f"Your mark achieved on {exam['exam']} will be disregarded to get this optimum mark.\n"

    return (optimum_mark,additional_info)

if __name__ == "__main__":

    print("")

    results = optimum_mark(exam_data("exams.csv"))

    print(f"Based on your estimates, you will achieve an optimum mark of \033[1m\033[4m{results[0]:.1f}\033[0m.\n")

    print(results[1])
