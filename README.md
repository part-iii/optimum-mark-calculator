# Optimum Mark Calculator

This is intended to give you an estimate of what you might achieve in the Part III, given the exams you want to take and your estimated grades. It does not take into account quality marks.

### What are the grade boundaries?

There aren't (currently) fixed grade boundaries for Part III. More information about the way Part III is assessed can be found in the [handbook](https://www.maths.cam.ac.uk/postgrad/part-iii/current/lectures-and-examples-classes#Handbook). As a rough guide, from [FOI requests](https://www.whatdotheyknow.com/request/cambridge_part_iii_maths_grade_s/), data from 2012-2022 show that
* a distinction corresponded to an optimum mark of 74.6 ± 0.7 or higher;
* a merit corresponded to an optimum mark of 65.8 ± 1.6 or higher;
* and a pass/honours corresponded to an optimum mark of 33.0 ± 1.8 or higher.

In 2023/24, a distinction was 75, which is not unusual, but the merit boundary was 70, and the pass boundary was 60. These latter grades are unusually high. This could mean one of a few things, but two important possibilities are that
1. the data from the FOI requests are not correct;
2. or, the grading procedure has changed.

I don't know if it's either of these, or what the issue is.

### How good is the average essay?

Again, [FOI requests](https://www.whatdotheyknow.com/request/part_iii_essay_results) give us the data, and from 2019-2023, the average essay mark was 77 ± 13. The data are slightly skewed to the higher end, with a median of 79, upper quartile of 87, and lower quartile of 68.

### What are quality marks and how do they affect my grade?

See the [handbook](https://www.maths.cam.ac.uk/postgrad/part-iii/current/lectures-and-examples-classes#Handbook) for some information. I'm not sure if the exact calculation is known to anyone as to how they affect your grade. For example, my results were boosted by 14 percentage points, which I can only assume was down to quality marks, whereas another student I know only got boosted by ~0.5 percentage points.

## Getting Started
0. Ensure you have a working `python3` setup
1. Download `exams.py` and `exams.csv`.
2. `exams.csv` is a template file. Edit it to reflect the modules you are taking to exam, leaving the top row unchanged. The file is a CSV, so do not add any additional commas. 
3. Run `exams.py`. It will output to the terminal an estimate and information regarding which, if any, of your exams have been disregarded.

## Example Output

This exam is using the `exams.csv` file on this repository.

    $ python3 exams.py

    Based on your estimates, you will achieve an optimum mark of 74.1.

    Your mark achieved on SIG will be disregarded to get this optimum mark.

